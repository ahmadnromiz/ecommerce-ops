#!/bin/sh
passwd=$(cat $USER_PASSWORD)
passwd1=''

sed -i "s|<APP_URL>|$APP_URL|g" $CI_PROJECT_DIR/image-build/fpm/web-data/.env
sed -i "s|<DB_USER>|$DB_USER|g" $CI_PROJECT_DIR/image-build/fpm/web-data/.env
sed -i "s|<USER_PASSWORD>|$passwd|g" $CI_PROJECT_DIR/image-build/fpm/web-data/.env
sed -i "s|<DB_HOSTNAME>|$DB_HOSTNAME|g" $CI_PROJECT_DIR/image-build/fpm/web-data/.env
sed -i "s|<REDIS_HOSTNAME>|$REDIS_HOSTNAME|g" $CI_PROJECT_DIR/image-build/fpm/web-data/.env

sed -i "s|<APP_URL>|$APP_URL|g" $CI_PROJECT_DIR/image-build/fpm/bagisto/.env
sed -i "s|<DB_USER>|$DB_USER|g" $CI_PROJECT_DIR/image-build/fpm/bagisto/.env
sed -i "s|<USER_PASSWORD>|$passwd1|g" $CI_PROJECT_DIR/image-build/fpm/bagisto/.env
sed -i "s|<DB_HOSTNAME>|$DB_HOSTNAME|g" $CI_PROJECT_DIR/image-build/fpm/bagisto/.env
sed -i "s|<NAME_REDIS>|$NAME_REDIS|g" $CI_PROJECT_DIR/image-build/fpm/bagisto/.env
sed -i "s|<DB_NAME>|$DB_NAME|g" $CI_PROJECT_DIR/image-build/fpm/bagisto/.env
