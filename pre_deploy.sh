#!/bin/bash

sed -i "s|<image-tag-name>|$IMAGE_TAG1|g" $MANIFEST_PATH/deploy/deployments/*.yaml
sed -i "s|<DB_NAME>|$DB_NAME|g" $MANIFEST_PATH/deploy/statefulsets/*.yaml
sed -i "s|<DB_USER>|$DB_USER|g" $MANIFEST_PATH/deploy/statefulsets/*.yaml
sed -i "s|<APP_URL>|$APP_URL|g" $MANIFEST_PATH/deploy/configmaps/*.yaml
sed -i "s|<APP_URL>|$APP_URL|g" $MANIFEST_PATH/deploy/ingresses/*.yaml && \
sed -i "s|<CERT_NAME>|$CERT_NAME|g" $MANIFEST_PATH/deploy/ingresses/*.yaml
sed -i "s|<NAME_INGRESS>|$NAME_INGRESS|g" $MANIFEST_PATH/deploy/ingresses/*.yaml
sed -i "s|<NAMESPACE>|$NAMESPACE|g" $MANIFEST_PATH/namespace/*.yaml

find $MANIFEST_PATH/deploy/ -type f -print0 | xargs -0 sed -i "s|<NAME_CONFIGMAP>|$NAME_CONFIGMAP|g" && \
find $MANIFEST_PATH/deploy/ -type f -print0 | xargs -0 sed -i "s|<NAMESPACE>|$NAMESPACE|g" && \
find $MANIFEST_PATH/deploy/ -type f -print0 | xargs -0 sed -i "s|<NAME_FPM>|$NAME_FPM|g" && \
find $MANIFEST_PATH/deploy/ -type f -print0 | xargs -0 sed -i "s|<NAME_NGINX>|$NAME_NGINX|g" && \
find $MANIFEST_PATH/deploy/ -type f -print0 | xargs -0 sed -i "s|<NAME_REDIS>|$NAME_REDIS|g"

