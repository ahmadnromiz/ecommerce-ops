#!/bin/bash

#VALUE1=$(kubectl -n $NAMESPACE get pvc | awk '/^db/ {print $1}')
#VALUE2=$(kubectl -n $NAMESPACE get pvc | awk '/^travellist/ {print $1}')

# Main Deploy Script
if [[ "$TAKEDOWN" =~ "/activate/" ]]; then
  kubectl "$KUBE_ACTION" -f "$MANIFEST_PATH"/namespace/ --recursive;
else
  kubectl "$KUBE_ACTION" -f "$MANIFEST_PATH"/namespace/ --recursive && \
  kubectl -n $NAMESPACE create secret generic database --from-file=user_password=$USER_PASSWORD
  kubectl -n $NAMESPACE create secret tls "$CERT_NAME" \
    --cert=ssl/fullchain.pem \
    --key=ssl/privkey.pem
  kubectl "$KUBE_ACTION2" -f "$MANIFEST_PATH"/deploy/statefulsets/ --recursive && sleep 1;
  kubectl "$KUBE_ACTION" -f "$MANIFEST_PATH"/deploy/ --recursive;
fi
